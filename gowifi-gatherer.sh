#!/bin/bash

mongoexport --db iplaypen-development --collection charges --fields app_id,shortcode,reference_code,amount,subscriber_number,status,created_at --csv --query '{app_id: 5069, created_at: {$gte: new Date(1554048000000), $lte: new Date(1556639999000)}, status: "SUCCESS"}' --out 21584001_success_april_2019_charges.csv
mongoexport --db iplaypen-development --collection charges --fields app_id,shortcode,reference_code,amount,subscriber_number,status,created_at --csv --query '{app_id: 5432, created_at: {$gte: new Date(1554048000000), $lte: new Date(1556639999000)}, status: "SUCCESS"}' --out 21586455_success_april_2019_charges.csv
mongoexport --db iplaypen-development --collection charges --fields app_id,shortcode,reference_code,amount,subscriber_number,status,created_at --csv --query '{app_id: 8730, created_at: {$gte: new Date(1554048000000), $lte: new Date(1556639999000)}, status: "SUCCESS"}' --out 21587568_success_april_2019_charges.csv

