import csv
from datetime import datetime

date_fmt = "%Y-%m-%d %H:%M:%S"
with open('loadup.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    with open('loadup-delay.csv', 'w') as csvfile2:
        fieldnames = reader.fieldnames
        fieldnames.append('delay')
        writer = csv.DictWriter(csvfile2, fieldnames)
        writer.writeheader()
        for row in reader:
            row['delay'] = datetime.strptime(row['updated_at'], date_fmt) - datetime.strptime(row['created_at'], date_fmt)
            print('created_at: {}, updated_at: {}, delay: {}'.format(row['created_at'], row['updated_at'], row['delay']))
            writer.writerow(row)
