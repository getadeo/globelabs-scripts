var param1 = arg1;
var param2 = arg2;

var count = db.logs.find({
  created_at: {
    "$gte": new ISODate(arg1),
    "$lte": new ISODate(arg2)
  }
}).count()

var data = "logs-" + arg1 + "," + count

print(data)

// printjson({ keyName: db.logs.find({created_at: {"$gte": new ISODate(arg1), "$lte": new ISODate(arg2)}}).count() })

// printjson( { "charges": db.charges.find({created_at: {"$gte": new ISODate(arg1), "$lte": new ISODate(arg2)}}).count() })
