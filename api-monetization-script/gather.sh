#!/bin/bash

for day in {1..28}
  do
    for hour in {0..23}
      do
        hourlength=$(echo ${#hour})
        if [ $hourlength -eq "1" ]
        then
          echo "Day $day, Hour: 0${hour}"
          # echo "0$hour"
          daylength=$(echo ${#day})
          if [ $daylength -eq "1" ]
          then
            fromDate="'2019-02-0${day}T0${hour}:00:00.000+08:00'"
            endDate="'2019-02-0${day}T0${hour}:59:59.999+08:00'"
            query="var arg1=${fromDate}, arg2=${endDate}"
          else
            fromDate="'2019-02-${day}T0${hour}:00:00.000+08:00'"
            endDate="'2019-02-${day}T0${hour}:59:59.999+08:00'"
            query="var arg1=${fromDate}, arg2=${endDate}"
          fi
          echo $query
          mongo iplaypen-development --eval "${query}" logs.js >> logs-feb${day}
          mongo iplaypen-development --eval "${query}" charges.js >> charges-feb${day}
        else
          echo "Day $day, Hour: $hour"
          # echo "$hour"
          daylength=$(echo ${#day})
          if [ $daylength -eq "1" ]
          then
            fromDate="'2019-02-0${day}T${hour}:00:00.000+08:00'"
            endDate="'2019-02-0${day}T${hour}:59:59.999+08:00'"
            query="var arg1=${fromDate}, arg2=${endDate}"
          else
            query="var arg1='2019-02-${day}T${hour}:00:00.000+08:00', arg2='2019-02-${day}T${hour}:59:59.999+08:00'"
          fi
          echo $query
          mongo iplaypen-development --eval "${query}" logs.js >> logs-feb${day}
          mongo iplaypen-development --eval "${query}" charges.js >> charges-feb${day}
        fi

      done
  done
