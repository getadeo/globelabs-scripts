import csv
import pymysql.cursors

connection = pymysql.connect(
    host='localhost',
    user='',
    password='',
    db='myippdb',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor)

filename="topups-january-march-2019.csv"
output_filename = "topups-january-to-march-2019.csv"
with open(filename, 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    with open(output_filename, 'w') as csvfile2:
        fieldnames = ['id', 'vault_id', 'email', 'amount', 'current_amount', 'expired_at', 'created_at', 'expired']
        writer = csv.DictWriter(csvfile2, fieldnames=fieldnames)
        writer.writeheader()
        try:
            with connection.cursor() as cursor:
                for row in reader:
                    sql = "SELECT `email` from `users` WHERE `wallet_id`=%s"
                    cursor.execute(sql, (row['vault_id'],))
                    result = cursor.fetchone()
                    row['amount'] = str(int(row['amount']) / 100)
                    row['current_amount'] = str(int(row['current_amount']) / 100)

                    if result is None:
                        row['email'] = 'mo_wallet'
                    else:
                        row['email'] = result['email']
                    print(f"Writing {row}")
                    writer.writerow(row)
        finally:
            connection.close()
