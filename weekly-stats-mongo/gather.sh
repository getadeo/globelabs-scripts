#!/bin/bash


# SUCCESS
echo 'success-logs-jan28-31'
time mongo iplaypen-development --eval "var arg1='2018-01-28T00:00:00.000+08:00', arg2='2018-01-31T23:59:59.999+08:00', statusarg='SUCCESS'" logs.js > success-logs-jan28-31

echo 'success-charges-jan28-31'
time mongo iplaypen-development --eval "var arg1='2018-01-28T00:00:00.000+08:00', arg2='2018-01-31T23:59:59.999+08:00', statusarg='SUCCESS'" charges.js > success-charges-jan28-31


# PENDING
echo 'pending-logs-jan28-31'
time mongo iplaypen-development --eval "var arg1='2018-01-28T00:00:00.000+08:00', arg2='2018-01-31T23:59:59.999+08:00', statusarg='PENDING'" logs.js > pending-logs-jan28-31

echo 'pending-charges-jan28-31'
time mongo iplaypen-development --eval "var arg1='2018-01-28T00:00:00.000+08:00', arg2='2018-01-31T23:59:59.999+08:00', statusarg='PENDING'" charges.js > pending-charges-jan28-31

# FAILED
echo 'failed-logs-jan28-31'
time mongo iplaypen-development --eval "var arg1='2018-01-28T00:00:00.000+08:00', arg2='2018-01-31T23:59:59.999+08:00', statusarg='FAILED'" logs.js > failed-logs-jan28-31

echo 'failed-charges-jan28-31'
time mongo iplaypen-development --eval "var arg1='2018-01-28T00:00:00.000+08:00', arg2='2018-01-31T23:59:59.999+08:00', statusarg='FAILED'" charges.js > failed-charges-jan28-31

