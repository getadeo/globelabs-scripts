var count = db.logs.find({
  shortcode_prefix: arg3,
  created_at: {
    "$gte": new ISODate(arg1),
    "$lte": new ISODate(arg2)
  }
}).count()

var data = "logs-" + arg1 + "," + count

print(data)