#!/bin/bash

targetdir='/home/ubuntu/rjangeles/Regen-SMSbyApp/'
applistfile='/home/ubuntu/rjangeles/Regen-SMSbyApp/Regen-SMS-App-FAILED.txt'
startdate=`date -d "-1 month -$(($(date +%d))) days 16:00:00" "+%s000"`
enddate=`date -d "-$(date +%d) days 15:59:59" "+%s000"`
month=`date -d "-$(date +%d) days 15:59:59" "+%b"`
year=`date -d "-$(date +%d) days 15:59:59" "+%Y"`

echo "$0" && while read appid; do query="{app_id: '$appid', 'status':'FAILED', 'created_at': {\$gte: new Date($startdate), \$lte: new Date($enddate)}}" && mongoexport --db iplaypen-development --collection logs --fields type,app_id,shortcode_suffix,shortcode_prefix,subscriber_number,number_of_messages_in_batch,status,telco,created_at,error --csv --query "$query" --out "$targetdir$appid-FAILED-$month-$year-logs.csv"; done < $applistfile