#!/bin/bash

dbhost='ippdb2prod-slave.cy98fbun2kho.ap-southeast-1.rds.amazonaws.com'
dbuser='ippuser'
dbpass='ippuser123$$'
dbdb='amax_production'
year='2018'
month='12'
starthour=':00:00'
endhour=':59:59'
filename="/home/ubuntu/rjangeles/count.per.day.per.hour/rewards.hourly.$year.$month.txt"

for dayofmonth in {1..31}; do
    dateofthemonth=`date -d "$year-$month-0$dayofmonth" '+%Y-%m-%d'`;
    dayofmonthminusone=`date -d "$dateofthemonth-1day" '+%Y-%m-%d'`;
    for hourofday in {16..23}; do
        echo -n $dayofmonthminusone $hourofday$starthour $hourofday$endhour" " >> $filename && mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from transaction where created_at between '$dayofmonthminusone $hourofday$starthour' and '$dayofmonthminusone $hourofday$endhour';" >> $filename;
    done;

    for hourofday in {0..9}; do
        echo -n $dateofthemonth "0"$hourofday$starthour "0"$hourofday$endhour" " >> $filename && mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from transaction where created_at between '$dateofthemonth 0$hourofday$starthour' and '$dateofthemonth 0$hourofday$endhour';" >> $filename;
    done;

    for hourofday in {10..15}; do
        echo -n $dateofthemonth $hourofday$starthour $hourofday$endhour" " >> $filename && mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from transaction where created_at between '$dateofthemonth $hourofday$starthour' and '$dateofthemonth $hourofday$endhour';" >> $filename;
    done;
done