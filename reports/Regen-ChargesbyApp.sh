#!/bin/bash

targetdir='~/Regen-ChargesbyApp/'
applistfile="$1"
startdate="$2"
enddate="$3"
month="$4"
year="$5"
sstatus='SUCCESS'
fstatus='FAILED'

while read appid; do query="{app_id: $appid, 'created_at': {\$gte: new Date($startdate), \$lte: new Date($enddate)}}" && mongoexport --db iplaypen-development --collection charges --fields app_id,shortcode,reference_code,amount,subscriber_number,status,created_at --csv --query "$query" --out "$targetdir$appid-$month-$year-charges.csv"; done < $applistfile
while read appid; do query="{app_id: $appid, 'created_at': {\$gte: new Date($startdate), \$lte: new Date($enddate)}, status: '$sstatus'}" && mongoexport --db iplaypen-development --collection charges --fields app_id,shortcode,reference_code,amount,subscriber_number,status,created_at --csv --query "$query" --out "$targetdir$appid-$month-$year-$sstatus-charges.csv"; done < $applistfile
while read appid; do query="{app_id: $appid, 'created_at': {\$gte: new Date($startdate), \$lte: new Date($enddate)}, status: '$fstatus'}" && mongoexport --db iplaypen-development --collection charges --fields app_id,shortcode,reference_code,amount,subscriber_number,status,created_at --csv --query "$query" --out "$targetdir$appid-$month-$year-$fstatus-charges.csv"; done < $applistfile

#45 02 22 02 5 bash -x ~/Regen-ChargesbyApp/Regen-ChargesbyApp.sh ~/Regen-ChargesbyApp/gowifi.txt 1546272000000 1548950399000 JAN 2019