#!/bin/bash
prefix="''"
directory=''
for day in {1..28}
  do
    daylength=$(echo ${#day})
    if [ $daylength -eq "1" ]
    then
      fromDate="'2019-02-0${day}T00:00:00.000+08:00'"
      endDate="'2019-02-0${day}T23:59:59.999+08:00'"
      query="var arg1=${fromDate}, arg2=${endDate}, arg3=${prefix}"
    else
      fromDate="'2019-02-${day}T00:00:00.000+08:00'"
      endDate="'2019-02-${day}T23:59:59.999+08:00'"
      query="var arg1=${fromDate}, arg2=${endDate}, arg3=${prefix}"
    fi
    echo $query
    mongo '' --eval "${query}" "${directory}logs.js" >> "${directory}logs-feb"
  done