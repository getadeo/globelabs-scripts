#!/bin/bash

#rewardsfile='~/rewards.extractor/apps.capterra'
rewardsfile="$1"

amaxhost=''
amaxuser=''
amaxpass=''
amaxdb=''
#startdate=`date -d '8 days ago' '+%Y-%m-%d'`
#enddate=`date -d '1 days ago' '+%Y-%m-%d'`
startdate="$2"
enddate="$3"
starthour='16:00:00'
endhour='15:59:59'
month="$4"
year="$5"

while read app_id; do mysql -h$amaxhost -u$amaxuser -p$amaxpass $amaxdb -e"select id, app_id, subscriber_msisdn, amount, status, convert_tz(created_at, '+00:00', '+08:00') as 'created_at +8', convert_tz(updated_at, '+00:00', '+08:00') as 'updated_at +8', amax_transaction_id, reward_keyword from transaction where app_id=${app_id} and created_at between '$startdate $starthour' and '$enddate $endhour'" | sed 's/\t/,/g' > "~/rewards.extractor/${app_id}-$month-$year.csv"; done < $rewardsfile

#crontab sample:
#55 08 12 03 2 bash -x ~/rewards.extractor/rewards.regen.sh ~/rewards.extractor/apps.capterra 2017-03-31 2017-04-30 APR 2017