#!/bin/bash

targetdir='~/rjangeles/Regen-SMSbyDate/'
startdate="$1"
enddate="$2"
apitype="$3"
prefix="$4"
stat="$5"

echo "$0" && query="{'type': '$apitype', 'shortcode_prefix': '$prefix', 'status':'$stat', 'created_at': {\$gte: new Date($startdate), \$lte: new Date($enddate)}}" && mongoexport --db iplaypen-development --collection logs --fields type,app_id,access_token,message_body,message_id,shortcode_suffix,shortcode_prefix,subscriber_number,number_of_messages_in_batch,status,telco,created_at,error --csv --query "$query" --out "$targetdir$startdate-$enddate-$apitype-$prefix-$stat-logs.csv"