#!/bin/bash

targetdir='~/rjangeles/Regen-LBSbyApp/'
applistfile="$1"
startdate="$2"
enddate="$3"
month="$4"
year="$5"
apitype="$6"

echo "$0" && while read appid; do query="{app_id: '$appid', 'type': '$apitype', 'created_at': {\$gte: new Date($startdate), \$lte: new Date($enddate)}}" && mongoexport --db iplaypen-development --collection logs --fields type,app_id,shortcode_suffix,shortcode_prefix,subscriber_number,status,created_at,error --csv --query "$query" --out "$targetdir$appid-$month-$year-logs.csv"; done < $applistfile