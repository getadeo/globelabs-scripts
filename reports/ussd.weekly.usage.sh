#!/bin/bash

dbhost='ippdb2prod-slave.cy98fbun2kho.ap-southeast-1.rds.amazonaws.com'
dbuser='ippuser'
dbpass='ippuser123$$'
dbdb='ussd_production'
startdate=`date -d '8 days ago' '+%Y-%m-%d'`
enddate=`date -d '1 day ago' '+%Y-%m-%d'`
starthour='16:00:00'
endhour='15:59:59'
filename="/home/ubuntu/rjangeles/ussd.weekly.$startdate.$enddate.txt"

echo $startdate $starthour $enddate $endhour > $filename
echo -n 'MO SUCCESS ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'MO' and status = 'SUCCESS' and created_at between '$startdate $starthour' and '$enddate $endhour';" >> $filename
echo -n 'MO PENDING ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'MO' and status = 'PENDING' and created_at between '$startdate $starthour' and '$enddate $endhour';" >> $filename
echo -n 'MO FAILED ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'MO' and status = 'FAILED' and created_at between '$startdate $starthour' and '$enddate $endhour' and not subscriber_num = null;" >> $filename
echo -n 'MT SUCCESS ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'MT' and status = 'SUCCESS' and created_at between '$startdate $starthour' and '$enddate $endhour';" >> $filename
echo -n 'MT PENDING ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'MT' and status = 'PENDING' and created_at between '$startdate $starthour' and '$enddate $endhour';" >> $filename
echo -n 'MT FAILED ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'MT' and status = 'FAILED' and created_at between '$startdate $starthour' and '$enddate $endhour';" >> $filename
echo -n 'NI SUCCESS ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'NI' and status = 'SUCCESS' and created_at between '$startdate $starthour' and '$enddate $endhour';" >> $filename
echo -n 'NI PENDING ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'NI' and status = 'PENDING' and created_at between '$startdate $starthour' and '$enddate $endhour';" >> $filename
echo -n 'NI FAILED ' >> $filename
mysql -h$dbhost -u$dbuser -p$dbpass $dbdb -NBe"select count(id) from requests where request_type = 'NI' and status = 'FAILED' and created_at between '$startdate $starthour' and '$enddate $endhour and not subscriber_num = null';" >> $filename