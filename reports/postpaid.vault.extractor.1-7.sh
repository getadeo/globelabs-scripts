#!/bin/bash

ipphost='ippdb2prod-slave.cy98fbun2kho.ap-southeast-1.rds.amazonaws.com'
ippuser='ippuser'
ipppass='ippuser123$$'
ippdb='myippdb'
ippdbquery='select wallet_id from users where postpaid=1 order by credit_limit desc;'
postpaidfile='/home/ubuntu/rjangeles/postpaid.vault.extractor/postpaid.vault.cl.desc'

vaulthost='ippvault-slave.cy98fbun2kho.ap-southeast-1.rds.amazonaws.com'
vaultuser='vaultuser'
vaultpass='vaultuser123$$'
vaultdb='vault_production'
startdate=`date -d '8 days ago' '+%Y-%m-%d'`
enddate=`date -d '1 days ago' '+%Y-%m-%d'`
starthour='16:00:00'
endhour='15:59:59'
month=`date -d '1 days ago' '+%b'`
year=`date -d '1 days ago' '+%Y'`
range='1-7'

mysql -h$ipphost -u$ippuser -p$ipppass $ippdb -NBe"$ippdbquery" > $postpaidfile

while read vault_id; do mysql -h$vaulthost -u$vaultuser -p$vaultpass $vaultdb -e"select * from transaction where vault_id=${vault_id} and created_at between '$startdate $starthour' and '$enddate $endhour'" | sed 's/\t/,/g' > "/home/ubuntu/rjangeles/postpaid.vault.extractor/${vault_id}-$month-$range-$year.csv"; done < $postpaidfile