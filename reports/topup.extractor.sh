#!/bin/bash

ipphost=''
ippuser=''
ipppass=''
ippdb=''
vaulthost=''
vaultuser=''
vaultpass=''
vaultdb=''
starthour='16:00:00'
endhour='15:59:59'
rewardsfile='~/topup.extractor/vault-rewards-only.csv'
smsfile='~/topup.extractor/vault-sms-only.csv'
ippdbquery="select distinct users.wallet_id from apps inner join users on apps.user_id = users.id where apps.scope like '%amax%' and apps.scope not like '%mt,mo%' and users.postpaid = false;"
mysql -h$ipphost -u$ippuser -p$ipppass $ippdb -NBe"$ippdbquery" > $rewardsfile
ippdbquery="select distinct users.wallet_id from apps inner join users on apps.user_id = users.id where apps.scope like '%mt,mo%' and apps.scope not like '%amax%' and users.postpaid = false;"
mysql -h$ipphost -u$ippuser -p$ipppass $ippdb -NBe"$ippdbquery" > $smsfile

startdate=`date -d '2019-01-31' '+%Y-%m-%d'`
enddate=`date -d '2019-02-15' '+%Y-%m-%d'`
year=`date -d '2019-02-15' '+%Y'`
month=`date -d '2019-02-15' '+%b'`
while read vault_id; do mysql -h$vaulthost -u$vaultuser -p$vaultpass $vaultdb -e"select * from topup where vault_id=${vault_id} and created_at between '$startdate $starthour' and '$enddate $endhour'" | sed 's/\t/,/g' > "~/topup.extractor/${vault_id}-$year-$month-topup-rewards.csv"; done < $rewardsfile
while read vault_id; do mysql -h$vaulthost -u$vaultuser -p$vaultpass $vaultdb -e"select * from topup where vault_id=${vault_id} and created_at between '$startdate $starthour' and '$enddate $endhour'" | sed 's/\t/,/g' > "~/topup.extractor/${vault_id}-$year-$month-topup-sms.csv"; done < $smsfile