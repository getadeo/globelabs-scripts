import csv
from datetime import datetime, timedelta

filename = 'capterra-logs-may-31-june-1.csv'
output = 'local-capterra-logs-may-31-june-1.csv'
date_fmt = "%Y-%m-%d %H:%M:%S" 

with open(filename) as csvfile:
    reader = csv.DictReader(csvfile)
    with open(output, 'w') as csvfile2:
        fieldnames = reader.fieldnames
        writer = csv.DictWriter(csvfile2, fieldnames=fieldnames)
        writer.writeheader()
        for row in reader:
            row['created_at'] = datetime.strptime(row['created_at'], date_fmt) + timedelta(hours=8)
            row['created_at'] = datetime.strftime(row['created_at'], date_fmt)
            if row['updated_at'] != 'NULL':
                row['updated_at'] = datetime.strptime(row['updated_at'], date_fmt) + timedelta(hours=8)
                row['updated_at'] = datetime.strftime(row['updated_at'], date_fmt)
            print(row)
            writer.writerow(row)
