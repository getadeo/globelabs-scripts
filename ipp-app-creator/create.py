import csv
import json
import requests
from random import choice
from string import hexdigits

count = 50
app_name = "SMDA"
user_id = "2975"
description = "SeriousMD assistant"
short_description = "SeriousMD assistant"
email_support = "support@seriousmd.com"
redirect_uri = "https://seriousmd.com/sms/globe_redirect2"
notify_uri = "https://seriousmd.com/sms/globe_notify"
category = "messaging"
scope = "mt,mo,awesome"
charging_notification = True
use_globe_for_smart_mt = True
use_passphrase = True
deactivated = False

url = 'http://54.179.185.124/v1/app/'

with open('result.csv', 'w') as csvfile:
    fieldnames = ['shortcode', 'app_id', 'app_secret', 'passphrase']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

    for i in range(1, count):
        payload = {
            "user_id": user_id,
            "name": app_name + str(100+i),
            "description": description,
            "short_description": short_description,
            "email_support": email_support,
            "redirect_uri": redirect_uri,
            "notify_uri": notify_uri,
            "category": category,
            "charging_notification": charging_notification,
            "use_globe_for_smart_mt": use_globe_for_smart_mt,
            "use_passphrase": use_passphrase,
            "scope": scope,
            "passphrase": ''.join(choice(hexdigits) for i in range(10)),
            "deactivated": deactivated,
        }
        print("Requesting:")
        print(payload)
        r = requests.post(url, json=payload)
        print(r.status_code)
        print(r.content)
        r_data = r.json()
        csv_data = {
            "shortcode": r_data['shortcode']['prefix'] + r_data['shortcode']['suffix'],
            "app_id": r_data['hashed_id'],
            "app_secret": r_data['app_secret'],
            "passphrase": r_data['passphrase'],
        }
        print("Writing:")
        print(csv_data)
        writer.writerow(csv_data)



