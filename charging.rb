require 'csv'
require 'active_support/all'
require 'time'

$fileName = "February2019"

def addStat(n)
	@count[n-1] += 1
end

def initVars
	#count for each hit
	@count = []
	#0-10
	@rate0,@rate1,@rate2,@rate2x5,@rate3,@rate4,@rate5,@rate8,@rate10 = [],[],[],[],[],[],[],[],[]
	#11-25
	@rate11,@rate12,@rate13,@rate14,@rate15,@rate16,@rate20,@rate25 = [],[],[],[],[],[],[],[]
	#30-62
	@rate30,@rate35,@rate40,@rate45,@rate48,@rate50,@rate59,@rate60,@rate62 = [],[],[],[],[],[],[],[],[]
	#70-105
	@rate70,@rate75,@rate80,@rate89,@rate90,@rate99,@rate100,@rate105, = [],[],[],[],[],[],[],[]
	#110-160
	@rate110,@rate120,@rate125,@rate135,@rate140,@rate149,@rate150,@rate160 = [],[],[],[],[],[],[],[]
	#180-270
	@rate180,@rate190,@rate200,@rate210,@rate225,@rate240,@rate250,@rate270 = [],[],[],[],[],[],[],[]
	#299-380
	@rate299,@rate300,@rate315,@rate330,@rate350,@rate360,@rate375,@rate380 = [],[],[],[],[],[],[],[]
	#400-500
	@rate400,@rate405,@rate420,@rate449,@rate450,@rate480,@rate499,@rate500 = [],[],[],[],[],[],[],[]
	#525-800
	@rate525,@rate540,@rate550,@rate599,@rate600,@rate625,@rate675,@rate700,@rate760,@rate800 = [],[],[],[],[],[],[],[],[],[]
	#825-2999
	@rate825,@rate900,@rate999,@rate1000,@rate1100,@rate1500,@rate2000,@rate2500,@rate2999 = [],[],[],[],[],[],[],[],[]
  #3000-8000
  @rate3000,@rate3500,@rate4000,@rate4500,@rate5000,@rate6000,@rate7000,@rate7500,@rate8000 = [],[],[],[],[],[],[],[],[]
  #9000-16000
  @rate9000,@rate9900,@rate10000,@rate11000,@rate12000,@rate12500,@rate15000,@rate16000 = [],[],[],[],[],[],[],[]
  #18000-30000
  @rate18000,@rate20000,@rate25000,@rate27000,@rate30000 = [],[],[],[],[]
  #31500-52500
  @rate31500,@rate33000,@rate36000,@rate37500,@rate40000,@rate42000,@rate48000,@rate52500 = [],[],[],[],[],[],[],[]
  #60000
  @rate60000,@rate76000,@rate80000,@rate100000 = [],[],[],[]
	
	for i in 0..32 do
		#count for each hit
		@count <<0; 
		#0-10
		@rate0 <<0; @rate1 <<0; @rate2 <<0; @rate2x5 <<0; @rate3 <<0; @rate4 <<0; @rate5 <<0; @rate8 << 0; @rate10 <<0
		#11-25
		@rate11 <<0;  @rate12 <<0; @rate13 <<0; @rate14 <<0; @rate15 <<0; @rate16 <<0; @rate20 <<0; @rate25 <<0
		#30-62
		@rate30 <<0; @rate35 <<0; @rate40 <<0; @rate45 <<0; @rate48 <<0; @rate50 <<0; @rate59 <<0; @rate60 <<0; @rate62 <<0 
		#70-105
		@rate70 <<0; @rate75 <<0; @rate80 <<0; @rate89 <<0; @rate90 <<0; @rate99 <<0; @rate100 <<0; @rate105 <<0
		#110-160
		@rate110 <<0; @rate120 <<0; @rate125 <<0; @rate135 <<0; @rate140 <<0; @rate149 <<0; @rate150 <<0; @rate160 <<0 
		#180-270
		@rate180 <<0; @rate190 <<0; @rate200 <<0; @rate210 <<0; @rate225 <<0; @rate240 <<0; @rate250 <<0; @rate270 <<0
		#299-380
		@rate299 <<0; @rate330 <<0; @rate360 <<0; @rate375 <<0; @rate300 <<0; @rate315 <<0; @rate350 <<0; @rate380 <<0
		#400-500
		@rate400 <<0; @rate405 <<0; @rate420 <<0; @rate449 <<0; @rate450 <<0; @rate480 <<0; @rate499 <<0; @rate500 <<0 
		#525-800
		@rate525 <<0; @rate540 <<0; @rate550 <<0; @rate599 <<0; @rate600 <<0; @rate625 <<0; @rate675 <<0; @rate700 <<0; @rate760 <<0; @rate800 <<0
		#825-2999
		@rate825 <<0; @rate900 <<0; @rate999 <<0; @rate1000 <<0; @rate1100 <<0; @rate1500 <<0; @rate2000 <<0; @rate2500 <<0; @rate2999 <<0		
    #3000-8000
    @rate3000 <<0; @rate3500 <<0; @rate4000 <<0; @rate4500 <<0; @rate5000 <<0; @rate6000 <<0; @rate7000 <<0; @rate7500 <<0; @rate8000 <<0
    #9000-16000
    @rate9000 <<0; @rate9900 <<0; @rate10000 <<0; @rate11000 <<0; @rate12000 <<0; @rate12500 <<0; @rate15000 <<0; @rate16000 <<0
    #18000-30000
    @rate18000 <<0; @rate20000 <<0; @rate25000 <<0; @rate27000 <<0; @rate30000 <<0
    #31500-52500
    @rate31500 <<0; @rate33000 <<0; @rate36000 <<0; @rate37500 <<0; @rate40000 <<0; @rate42000 <<0; @rate48000 <<0; @rate52500 <<0
    #60000
    @rate60000 <<0; @rate76000 <<0; @rate80000 <<0; @rate100000 <<0
	end
end

def start
fileName = $fileName

Time.zone = "Asia/Hong_Kong"
	#Time.zone = "America/Juneau"
	t1 = Time.now
	ipp = []
	  puts "Reading " + fileName + ".csv"
		CSV.foreach(fileName+'.csv', "r") do |i|
			ipp << i
		end
	puts "Writing " + fileName + "_charges.csv"
	CSV.open(fileName+'_charges.csv', "a")do |logs|
		ipp.each_with_index do |x,index|
      puts "Writing #{x}" 
			date = x[6].to_time.in_time_zone
				logs << [x[1][4..7],x[4],x[6],x[5],date,x[3]]
		end
	end
	t2 = Time.now
	puts "-----------"
	puts "Start Time:"
	puts t1.to_s
	puts "End Time:"
	puts t2.to_s
	puts "-----------"



#fileName here <month>
#Init Variables
temp1,allShortcodes,uniqueShortcodes,logFile = [],[],[],[]

#Get shortcodes
puts "Reading " + fileName + "_charges.csv"
CSV.foreach(fileName+'_charges.csv',"r") {|i| logFile << i}
logFile.each {|x| allShortcodes << x[0]}
allShortcodes.uniq.each {|x| uniqueShortcodes << x}

#Call InitVars
initVars
		uniqueShortcodes.each do |sc|
		for i in 0..31 do
			logFile.each do |x|
				if sc.to_s.eql? x[0].to_s
				# For specific app id below:
				# if "6016".eql? x[0].to_s
				date = x[4] ; date = date.split('-')[8..9]
				rate = x[5].to_i/100 ; rate2 = x[5].to_i/100.to_f
					case date.to_i
						when i
						addStat(i)
						case rate
								when 0 ; @rate0[i-1] += 1
								when 1 ; @rate1[i-1] += 1
								when 2 ; rate2 == 2.5 ? (@rate2x5[i-1] += 1) : (@rate2[i-1] += 1)
								when 3 ; @rate3[i-1] += 1
								when 4 ; @rate4[i-1] += 1
								when 5 ; @rate5[i-1] += 1
								when 8 ; @rate8[i-1] += 1
								when 10 ; @rate10[i-1] += 1
								when 11 ; @rate11[i-1] += 1
								when 12 ; @rate12[i-1] +=1
								when 13 ; @rate13[i-1] += 1
								when 14 ; @rate14[i-1] += 1
								when 15 ; @rate15[i-1] += 1
								when 16 ; @rate16[i-1] += 1
								when 20 ; @rate20[i-1] += 1
								when 25 ; @rate25[i-1] += 1
								when 30 ; @rate30[i-1] += 1
								when 35 ; @rate35[i-1] += 1
								when 40 ; @rate40[i-1] += 1
								when 45 ; @rate45[i-1] += 1
								when 48 ; @rate48[i-1] += 1
								when 50 ; @rate50[i-1] += 1
								when 59 ; @rate59[i-1] += 1
								when 60 ; @rate60[i-1] += 1
								when 62 ; @rate62[i-1] += 1
								when 70 ; @rate70[i-1] += 1
								when 75 ; @rate75[i-1] += 1
								when 80 ; @rate80[i-1] += 1
								when 89 ; @rate89[i-1] += 1
								when 90 ; @rate90[i-1] += 1
								when 99 ; @rate99[i-1] += 1
								when 100 ; @rate100[i-1] += 1
								when 105 ; @rate105[i-1] += 1
								when 110 ; @rate110[i-1] += 1
								when 120 ; @rate120[i-1] += 1
								when 125 ; @rate125[i-1] += 1
								when 135 ; @rate135[i-1] += 1
								when 140 ; @rate140[i-1] += 1
								when 149 ; @rate149[i-1] += 1
								when 150 ; @rate150[i-1] += 1
								when 160 ; @rate160[i-1] += 1
								when 180 ; @rate180[i-1] += 1
								when 190 ; @rate190[i-1] += 1	
								when 200 ; @rate200[i-1] += 1
								when 210 ; @rate210[i-1] += 1
								when 225 ; @rate225[i-1] += 1
								when 240 ; @rate240[i-1] += 1
								when 250 ; @rate250[i-1] += 1
								when 270 ; @rate270[i-1] += 1
								when 299 ; @rate299[i-1] += 1
								when 300 ; @rate300[i-1] += 1
								when 315 ; @rate315[i-1] += 1
								when 330 ; @rate330[i-1] += 1
								when 350 ; @rate350[i-1] += 1
								when 360 ; @rate360[i-1] += 1
								when 375 ; @rate375[i-1] += 1
								when 380 ; @rate380[i-1] += 1
								when 400 ; @rate400[i-1] += 1
								when 405 ; @rate405[i-1] += 1
								when 420 ; @rate420[i-1] += 1
								when 449 ; @rate449[i-1] += 1	
								when 450 ; @rate450[i-1] += 1
								when 480 ; @rate480[i-1] += 1
								when 499 ; @rate499[i-1] += 1
								when 500 ; @rate500[i-1] += 1
								when 525 ; @rate525[i-1] += 1
								when 540 ; @rate540[i-1] += 1
								when 550 ; @rate550[i-1] += 1
								when 599 ; @rate599[i-1] += 1
								when 600 ; @rate600[i-1] += 1
								when 625 ; @rate625[i-1] += 1
								when 675 ; @rate675[i-1] += 1
								when 700 ; @rate700[i-1] +=1
								when 760 ; @rate760[i-1] +=1
                				when 800 ; @rate800[i-1] +=1
								when 825 ; @rate825[i-1] += 1
								when 900 ; @rate900[i-1] += 1
								when 999 ; @rate999[i-1] +=1
								when 1000 ; @rate1000[i-1] += 1
								when 1100 ; @rate1100[i-1] += 1
								when 1500 ; @rate1500[i-1] += 1
								when 2000 ; @rate2000[i-1] += 1
								when 2500 ; @rate2500[i-1] += 1
								when 2999 ; @rate2999[i-1] += 1
								when 3000 ; @rate3000[i-1] += 1
								when 3500 ; @rate3500[i-1] += 1
								when 4000 ; @rate4000[i-1] += 1
								when 4500 ; @rate4500[i-1] += 1
								when 5000 ; @rate5000[i-1] += 1
								when 6000 ; @rate6000[i-1] += 1
								when 7000 ; @rate7000[i-1] += 1
								when 7500 ; @rate7500[i-1] += 1
								when 8000 ; @rate8000[i-1] += 1
								when 9000 ; @rate9000[i-1] += 1
								when 9900 ; @rate9900[i-1] += 1
								when 10000 ; @rate10000[i-1] += 1
								when 11000 ; @rate11000[i-1] += 1
								when 12000 ; @rate12000[i-1] += 1
								when 12500 ; @rate12500[i-1] += 1
								when 15000 ; @rate15000[i-1] += 1
								when 16000 ; @rate16000[i-1] += 1
								when 18000 ; @rate18000[i-1] += 1
								when 20000 ; @rate20000[i-1] += 1
								when 25000 ; @rate25000[i-1] += 1
								when 27000 ; @rate27000[i-1] += 1
								when 30000 ; @rate30000[i-1] += 1
								when 31500 ; @rate31500[i-1] += 1
								when 33000 ; @rate33000[i-1] += 1
								when 36000 ; @rate36000[i-1] += 1
								when 37500 ; @rate37500[i-1] += 1
								when 40000 ; @rate40000[i-1] += 1
								when 42000 ; @rate42000[i-1] += 1
								when 48000 ; @rate48000[i-1] += 1
								when 52500 ; @rate52500[i-1] += 1
								when 60000 ; @rate60000[i-1] += 1
								when 76000 ; @rate76000[i-1] += 1
								when 80000 ; @rate80000[i-1] += 1
								when 100000 ; @rate100000[i-1] += 1
								end
						end
					end
				end
		end
		puts "Writing " + fileName + "_ok.csv"
		CSV.open(fileName+'_ok.csv', "a")do |logs|
					
			logs << [sc,"0",@rate0.inject(:+)] ; logs << ["","1",@rate1.inject(:+)]
			logs << ["","2",@rate2.inject(:+)]
			logs << ["","2.5",@rate2x5.inject(:+)]
			logs << ["","3",@rate3.inject(:+)]
			logs << ["","4",@rate4.inject(:+)]
			logs << ["","5",@rate5.inject(:+)]
			logs << ["","8",@rate8.inject(:+)]
			logs << ["","10",@rate10.inject(:+)]
			logs << ["","11",@rate11.inject(:+)]
			logs << ["","12",@rate12.inject(:+)]
			logs << ["","13",@rate13.inject(:+)]
			logs << ["","14",@rate14.inject(:+)]
			logs << ["","15",@rate15.inject(:+)]
			logs << ["","16",@rate16.inject(:+)]
			logs << ["","20",@rate20.inject(:+)]
			logs << ["","25",@rate25.inject(:+)]
			logs << ["","30",@rate30.inject(:+)]
			logs << ["","35",@rate35.inject(:+)]
			logs << ["","40",@rate40.inject(:+)]
			logs << ["","45",@rate45.inject(:+)]
			logs << ["","48",@rate48.inject(:+)]
			logs << ["","50",@rate50.inject(:+)]
			logs << ["","59",@rate59.inject(:+)]
			logs << ["","60",@rate60.inject(:+)]
			logs << ["","62",@rate62.inject(:+)]
			logs << ["","70",@rate70.inject(:+)]
			logs << ["","75",@rate75.inject(:+)]
			logs << ["","80",@rate80.inject(:+)]
			logs << ["","89",@rate89.inject(:+)]
			logs << ["","90",@rate90.inject(:+)]
			logs << ["","99",@rate99.inject(:+)]
			logs << ["","100",@rate100.inject(:+)]
			logs << ["","105",@rate105.inject(:+)]
			logs << ["","110",@rate110.inject(:+)]
			logs << ["","120",@rate120.inject(:+)]
			logs << ["","125",@rate125.inject(:+)]
			logs << ["","135",@rate135.inject(:+)]
			logs << ["","140",@rate140.inject(:+)]
			logs << ["","149",@rate149.inject(:+)]
			logs << ["","150",@rate150.inject(:+)]
			logs << ["","160",@rate160.inject(:+)]
			logs << ["","180",@rate180.inject(:+)]
			logs << ["","190",@rate190.inject(:+)]
			logs << ["","200",@rate200.inject(:+)]
			logs << ["","210",@rate210.inject(:+)]
			logs << ["","225",@rate225.inject(:+)]
			logs << ["","240",@rate240.inject(:+)]
			logs << ["","250",@rate250.inject(:+)]
			logs << ["","270",@rate270.inject(:+)]
			logs << ["","299",@rate299.inject(:+)]
			logs << ["","300",@rate300.inject(:+)]
			logs << ["","315",@rate315.inject(:+)]
			logs << ["","330",@rate330.inject(:+)]
			logs << ["","350",@rate350.inject(:+)]
			logs << ["","360",@rate360.inject(:+)]
			logs << ["","375",@rate375.inject(:+)]
			logs << ["","380",@rate380.inject(:+)]
			logs << ["","400",@rate400.inject(:+)]
			logs << ["","405",@rate405.inject(:+)]
			logs << ["","420",@rate420.inject(:+)]
			logs << ["","449",@rate449.inject(:+)]
			logs << ["","450",@rate450.inject(:+)]
			logs << ["","480",@rate480.inject(:+)]
			logs << ["","499",@rate499.inject(:+)]
			logs << ["","500",@rate500.inject(:+)]
			logs << ["","525",@rate525.inject(:+)]
			logs << ["","540",@rate540.inject(:+)]
			logs << ["","550",@rate550.inject(:+)]
			logs << ["","599",@rate599.inject(:+)]
			logs << ["","600",@rate600.inject(:+)]
			logs << ["","625",@rate625.inject(:+)]				
			logs << ["","675",@rate675.inject(:+)]
			logs << ["","700",@rate700.inject(:+)]
			logs << ["","760",@rate760.inject(:+)]
			logs << ["","800",@rate800.inject(:+)]
			logs << ["","825",@rate825.inject(:+)]
			logs << ["","900",@rate900.inject(:+)]
			logs << ["","999",@rate999.inject(:+)]
			logs << ["","1000",@rate1000.inject(:+)]
			logs << ["","1100",@rate1100.inject(:+)]
			logs << ["","1500",@rate1500.inject(:+)]
			logs << ["","2000",@rate2000.inject(:+)]
			logs << ["","2500",@rate2500.inject(:+)]
			logs << ["","2999",@rate2999.inject(:+)]
			logs << ["","3000",@rate3000.inject(:+)]
			logs << ["","3500",@rate3500.inject(:+)]
			logs << ["","4000",@rate4000.inject(:+)]
			logs << ["","4500",@rate4500.inject(:+)]
			logs << ["","5000",@rate5000.inject(:+)]
			logs << ["","6000",@rate6000.inject(:+)]
			logs << ["","7000",@rate7000.inject(:+)]
			logs << ["","7500",@rate7500.inject(:+)]
			logs << ["","8000",@rate8000.inject(:+)]
			logs << ["","9000",@rate9000.inject(:+)]
			logs << ["","9900",@rate9900.inject(:+)]
			logs << ["","10000",@rate10000.inject(:+)]
			logs << ["","11000",@rate11000.inject(:+)]
			logs << ["","12000",@rate12000.inject(:+)]
			logs << ["","12500",@rate12500.inject(:+)]
			logs << ["","15000",@rate15000.inject(:+)]
			logs << ["","16000",@rate16000.inject(:+)]
			logs << ["","18000",@rate18000.inject(:+)]
			logs << ["","20000",@rate20000.inject(:+)]
			logs << ["","25000",@rate25000.inject(:+)]
			logs << ["","27000",@rate27000.inject(:+)]
			logs << ["","30000",@rate30000.inject(:+)]
			logs << ["","31500",@rate31500.inject(:+)]
			logs << ["","36000",@rate36000.inject(:+)]
			logs << ["","37500",@rate37500.inject(:+)]
			logs << ["","40000",@rate40000.inject(:+)]
			logs << ["","42000",@rate42000.inject(:+)]
			logs << ["","48000",@rate48000.inject(:+)]
			logs << ["","52500",@rate52500.inject(:+)]
			logs << ["","60000",@rate60000.inject(:+)]
			logs << ["","76000",@rate76000.inject(:+)]
			logs << ["","80000",@rate80000.inject(:+)]
			logs << ["","100000",@rate100000.inject(:+)]
		end
		#Reset count
		initVars
	end

	#Insert to temp csv
	puts "Insert to temp csv: " + fileName + "_ok.csv"
	ipp = []
	CSV.foreach(fileName+'_ok.csv', "r") do |i|
		ipp << i
	end

	#filter by shortcode
	puts "Writing " + fileName + "_shortcode.csv"
	CSV.open(fileName+'_shortcode.csv', "a")do |logs|
		ipp.each do |x|
			if !x[0].empty?
				logs << [x[0],x[1],x[2]]
			elsif x[0].empty? and !x[2].eql? '0'
				logs << [x[0],x[1],x[2]]
			end
		end
	end

	#Filter by day
	puts "Writing " + fileName + "_stats.csv"
	CSV.open(fileName+'_stats.csv', "a")do |logs|
		for i in 1..31 do
		total_count, total_amount = 0,0
			logFile.each do |x|
			date = x[4]
			date = date.split('-')[2][0..1]
			rate = x[5].to_i/100.to_f
				if date.to_i.eql? i
					total_count += 1
					total_amount += rate
				end
			end
			logs << [i,total_count,total_amount]
		end	
	end
end

#filename without extension
start
