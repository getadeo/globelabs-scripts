import csv
from datetime import datetime, timedelta

filename = 'gowifi_success_november_2018_charges.csv'
output = 'local_gowifi_success_november_2018_charges.csv'
date_fmt = "%Y-%m-%dT%H:%M:%S.%fZ" 

with open(filename) as csvfile:
    reader = csv.DictReader(csvfile)
    with open(output, 'w') as csvfile2:
        fieldnames = reader.fieldnames
        writer = csv.DictWriter(csvfile2, fieldnames=fieldnames)
        writer.writeheader()
        for row in reader:
            row['created_at'] = datetime.strptime(row['created_at'], date_fmt) + timedelta(hours=8)
            row['created_at'] = datetime.strftime(row['created_at'], date_fmt)
            row['amount'] = int(row['amount']) / 100
            print(row)
            writer.writerow(row)
