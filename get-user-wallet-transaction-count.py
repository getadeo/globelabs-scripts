import csv
import pymysql 

connection = pymysql.connect(host='localhost',
        user='vaultuser',
        password='vaultuser123$$',
        db='vault_production',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor)

filename = "wallet_ids.csv"
output_filename = "wallet_counts.csv"
with open(filename, 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    with open(output_filename, 'w') as csvfile2:
        fieldnames = ['wallet_id','count']
        writer = csv.DictWriter(csvfile2, fieldnames=fieldnames)
        writer.writeheader()
        try:
            with connection.cursor() as cursor:
                for row in reader:
                    sql = "SELECT count(`id`) as count FROM `transaction` WHERE `vault_id`=%s AND (`created_at` BETWEEN '2017-01-01 16:00:00' AND '2017-12-31 15:59:59')"
                    cursor.execute(sql, (row['wallet_id'],))
                    result = cursor.fetchone()
                    row['count'] = result['count']
                    print("Writing: id: {}, count: {}".format(row['wallet_id'], row['count']))
                    writer.writerow(row)
        finally:
            connection.close()
