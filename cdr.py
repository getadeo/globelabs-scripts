import json
import csv

filename = 'Sep2018.csv'
with open('out_sep2018.csv', 'w') as csvfile:
    fieldnames = [
        'AccountID',
        'ApplicationId',
        'Called',
        'Caller',
        'Status',
        'StartTime',
        'EndTime',
        'Duration',
        'Flags',
        'RecordingDuration'
    ]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames, extrasaction='ignore')
    writer.writeheader()
    for line in open(filename, 'r'):
        ds = json.loads(line[1:-2])
        print("Writing:")
        print(
            ds['AccountID'],
            ds['ApplicationId'], 
            ds['Called'],
            ds['Caller'],
            ds['Status'],
            ds['StartTime'],
            ds['EndTime'],
            ds['Duration'],
            ds['Flags'],
            ds['RecordingDuration'],
        )
        writer.writerow(ds)
